import {Component, Input, OnInit} from '@angular/core';
import {Form} from "@angular/forms";
import {ToastrNotificationService} from "../../services/notification/toastr.notification.service";

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent implements OnInit {

  @Input() _from: Form;
  @Input() _name: string;
  @Input() _model: any = {};
  @Input() _formData: any;
  @Input() _allowedFile: any = null;

  public previewUrl:any = [];
  public fileUploadProgress: string = null;
  public uploadedFilePath: string = null;
  public FILESIZE = 5242880;
  public file;
  public files: any = [];
  private type;

  constructor(
    private toastr: ToastrNotificationService,
  ) { }

  ngOnInit() {
  }

  fileProgress(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        this.file = event.target.files[i];
        if(this.canUpload()){
          var reader = new FileReader();
          reader.onload = (event:any) => {
              this.previewUrl.push({type: this.type, path: event.target.result});
          }
          this.files.push(<File>this.file);
          reader.readAsDataURL(this.file);
          this.render();
        }
      }
    }
  }

  render = () => {
    this.files.forEach(item => {
      this._formData.append(`${this._name}`, item);
    });
  }

  remove = (index) => {
    this.files.splice(index, 1);
    this.previewUrl.splice(index, 1);
    this._formData.delete(`${this._name}`);
    this.render();
  }

  checkType = (type=null) => {
    switch ((type)?type:this.file.type) {
      case 'image/png':
      case 'image/gif':
      case 'image/jpeg':
      case 'image/pjpeg':
        console.log('image');
        this.type='image'
        return true;
        break;
      //case 'text/plain':
      //case 'text/html':
      //case 'application/x-zip-compressed':
      case 'application/pdf':
        this.type='pdf'
        console.log('PDF');
        return true;
        break;
      case 'application/msword':
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        this.type='word'
        console.log('word');
        return true;
        break;
      case 'application/vnd.ms-excel':
      case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        this.type='csv';
        console.log('CSV');
        return true;
        break;
      case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        this.type='ppt';
        console.log('PPT');
        return true;
        break;
      case 'video/mp4':
        this.type='video';
        console.log('Video');
        return true;
        break;
      default:
        this.toastr.showInfo('Unsupported file type!');
        return false;
    }
  }
  isAllowSize = () => {
    if (this.file.size > this.FILESIZE) { //Allowed file size is less than 5 MB (1048576)
      this.toastr.showInfo("<b>" + this.bytesToSize() + "</b> Too big file! <br />File is too big, it should be less than 5 MB.");
      return false;
    }
    return true;
  }

  canUpload = () => {
    return this.checkType() && this.isAllowSize();
  }

   bytesToSize = () => { //function to format bites bit.ly/19yoIPO
      var bytes = parseInt(this.file.size);
      var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      if (bytes == 0)
        return '0 Bytes';
      var i = Math.floor( Math.log(bytes) / Math.log(1024));
      return `${Math.round(bytes / Math.pow(1024, i))} ${sizes[i]}`;
  }

}
