import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NgxSmartModalModule } from 'ngx-smart-modal';
import { FormsModule } from '@angular/forms';
import {
  MatCheckboxModule,
  MatIconModule,
  MatButtonModule
} from '@angular/material';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule, routingComponents } from '../app-routing.module';
import { SpinnerLoaderComponent } from './spinner-loader/spinner-loader.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { AlertComponent } from './alert/alert.component';
import { UploaderComponent } from './uploader/uploader.component';

@NgModule({
  declarations: [
    SpinnerLoaderComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    LayoutComponent,
    AlertComponent,
    UploaderComponent
  ],
  imports: [
    CommonModule,
    //    NgxSmartModalModule.forRoot(),
    // AngularFontAwesomeModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    AppRoutingModule,
    FormsModule
  ],
  exports: [
    SpinnerLoaderComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    LayoutComponent,
    UploaderComponent
  ]
})
export class ShareModuleModule {}
