import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from '../../services/user.service';
import { CoreService } from '../../services/core/core.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  public userName: string;
  projectId: any;
  buttonStatus = false;
  menuClass = 'navi-icon';
  openSideBar = '';
  openMask = '';

  constructor(
    private currentUser: UserService,
    private coreService: CoreService
  ) {
    this.userName = this.currentUser.getUserName();
  }

  ngOnInit() {}
}
