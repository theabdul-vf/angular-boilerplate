import { Component, OnInit } from '@angular/core';
import {CoreService} from "../../services/core/core.service";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(
    public coreService: CoreService
  ) { }

  ngOnInit() {
  }

}
