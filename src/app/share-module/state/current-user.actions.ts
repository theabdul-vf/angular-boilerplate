import { Action } from '@ngrx/store';


export enum CurrentUserActionTypes {
  ADD_CURRENT_USER = '[Current User] Load Current User Success',
  REMOVE_CURRENT_USER = '[Current User] Remove Current User Success',
  GET_CURRENT_USER = '[Current User] Get Current User Success',
}

export class AddCurrentUser implements Action {
  readonly type = CurrentUserActionTypes.ADD_CURRENT_USER;
  constructor(public payload: any) {}
}

export class GetCurrentUser implements Action {
  readonly type = CurrentUserActionTypes.GET_CURRENT_USER;
  constructor(public payload: any) {}
}

export class RemoveCurrentUser implements Action {
  readonly type = CurrentUserActionTypes.REMOVE_CURRENT_USER;
  constructor(public payload: any) {}
}

export type CurrentUserActions = AddCurrentUser | RemoveCurrentUser;
