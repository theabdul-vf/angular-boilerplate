import { CurrentUserActionTypes, CurrentUserActions } from './current-user.actions';

export let initialState = []

export function currentUserReducer(state= initialState, action: CurrentUserActions) {
  switch (action.type) {
    case CurrentUserActionTypes.ADD_CURRENT_USER:
      return [...state, action.payload];
    case CurrentUserActionTypes.REMOVE_CURRENT_USER:
      const user = action.payload;
      return state.filter((el) => el.id !== user.id);
    default:
      return [...state];
  }
}
