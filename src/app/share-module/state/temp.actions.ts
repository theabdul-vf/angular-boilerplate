import { Action } from '@ngrx/store';


export enum TempActionTypes {
  CREATE_TEMP_MODEL = 'CREATE_TEMP_MODEL',
  GET_TEMP_MODEL = 'GET_TEMP_MODEL',
  REMOVE_TEMP_MODEL = 'REMOVE_TEMP_MODEL',
}

export class CreateTempModel implements Action {
  readonly type = TempActionTypes.CREATE_TEMP_MODEL;
  constructor(public payload: any) {}
}

export class GetTempModel implements Action {
  readonly type = TempActionTypes.GET_TEMP_MODEL;
  constructor(public payload: any) {}
}

export class RemoveTempModel implements Action {
  readonly type = TempActionTypes.REMOVE_TEMP_MODEL;
  constructor(public payload: any) {}
}

export type TempModel = CreateTempModel | GetTempModel | RemoveTempModel;
