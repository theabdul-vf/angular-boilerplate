import { TempActionTypes, TempModel } from './temp.actions';

export let initialState = []

export function tempModelReducer(state= initialState, action: TempModel) {
  switch (action.type) {
    case TempActionTypes.CREATE_TEMP_MODEL:
      return [...state, action.payload];
    case TempActionTypes.REMOVE_TEMP_MODEL:
      const data = action.payload;
      return state.filter((el) => el.id !== data.id);
    default:
      return [...state];
  }
}
