import { Component, OnInit } from '@angular/core';
import { CoreService } from '../../services/core/core.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor(public coreService: CoreService) {}

  ngOnInit() {}
}
