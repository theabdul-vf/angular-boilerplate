import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { appRoutes } from '../../app.constants';
import { UserService } from '../../services/user.service';
import { first } from 'rxjs/operators';
import { UiBlockerService } from '../../services/ui-blocker.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { ToastrNotificationService } from '../../services/notification/toastr.notification.service';
import { LabelService } from '../../services/core/label.service';
import { CoreService } from '../../services/core/core.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  projectId: number;
  public projectSelect: any;
  public projects = [];
  public errorMsg;
  public ellipsisName = 'ellipsis-v';
  public btn = false;
  public testBtn = 'testclass';
  public selectedItem: any;
  public user: any = {};
  public pageTitle;

  constructor(
    private currentUser: UserService,
    private uiBlockerService: UiBlockerService,
    private route: ActivatedRoute,
    private toastr: ToastrNotificationService,
    private labelService: LabelService,
    public coreService: CoreService,
    private router: Router
  ) {
    if (!currentUser.isLoggedIn()) {
      // .location.href = '/' + appRoutes.login;
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.pageTitle = this.coreService.getPageTitle();
  }
}
