import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuardService } from './services/auth/auth.guard.service';
import { LoginComponent } from './modules/login/login.component';
import { LogoutComponent } from './modules/logout/logout.component';
import { appRoutes } from './app.constants';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: appRoutes.logout, component: LogoutComponent }

  /* {
    path: ':tenant',
    children: [
      {
        path: appRoutes.project.projects,
        component: ProjectListComponent,
        canActivate: [AuthGuardService], children: [
        ] },
    ]
  },*/

  // { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [LoginComponent];
