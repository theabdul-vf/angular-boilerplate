import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
  AppRoutingModule
  // routingComponents
} from './app-routing.module';
import { AppComponent } from './app.component';

import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';

// import { MatCheckboxModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockUIModule } from 'ng-block-ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ng6-toastr-notifications';
import { AuthenticationService } from './services/authentication.service';
import { AlertService } from './services/alert.service';
import { UiBlockerService } from './services/ui-blocker.service';
import { LocalStorageService } from './services/local-storage.service';
import { AuthGuardService } from './services/auth/auth.guard.service';

import { LoginComponent } from '../app/modules/login/login.component';
import { LogoutComponent } from './modules/logout/logout.component';
import { StoreModule } from '@ngrx/store';
// import { EffectsModule } from '@ngrx/effects';
// import { StoreDevtoolsModule } from '@ngrx/store-devtools';
// import {
//   StoreRouterConnectingModule,
//   routerReducer,
//   RouterStateSerializer
// } from '@ngrx/router-store';
// import { AlertComponent } from '../app/modules/core/alert/alert.component';
import { currentUserReducer } from './share-module/state/current-user.reducer';
import { tempModelReducer } from './share-module/state/temp.reducer';
import { ToastrNotificationService } from './services/notification/toastr.notification.service';

import { LabelService } from './services/core/label.service';
import { ShareModuleModule } from './share-module/share-module.module';

import { ModulesModule } from './modules/modules.module';
import { MustMatchDirective } from './directives/must-match.directive';
import { CoreService } from './services/core/core.service';

@NgModule({
  declarations: [AppComponent, LogoutComponent, LoginComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BlockUIModule.forRoot(),
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ShareModuleModule,
    ModulesModule
    // StoreModule.forRoot({
    //   router: routerReducer,
    //   currentUser: currentUserReducer,
    //   tempModel: tempModelReducer
    // })
  ],
  providers: [
    AuthenticationService,
    AlertService,
    UiBlockerService,
    LocalStorageService,
    AuthGuardService,
    ToastrNotificationService,
    LabelService,
    CoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
