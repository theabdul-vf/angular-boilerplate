import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as StoreCurrentUser from '../share-module/state/current-user.actions';
import { LocalStorageService } from '../services/local-storage.service';

export enum UserConst {
  CURRENT_USER = '_currentUser',
  TOKEN = '_token',
  DEFAULT_PROJECT = '_default_project',
  DEFAULT_PROJECT_TYPE = '_default_project_type',
  IS_LOGGED_IN = '_isLoggedIn',
  USER_NAME = '_userName',
  PERMISSIONS = '_permissions'
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user;
  public storeUser: Observable<any>;
  private salt = '123456$#@$^@1ERF';

  constructor(private localStorageService: LocalStorageService) {
    this.user = this.localStorageService.getStringifyItem(
      UserConst.CURRENT_USER
    );
  }

  setUser = data => {
    this.localStorageService.setStringifyItem(UserConst.CURRENT_USER, data);
  };

  getUser = () => {
    return this.user;
  };

  setToken = token => {
    this.localStorageService.setItem(UserConst.TOKEN, token);
  };

  getToken = () => {
    return this.localStorageService.getItem(UserConst.TOKEN);
  };

  setIsLoggedIn = data => {
    this.localStorageService.setItem(UserConst.IS_LOGGED_IN, data);
  };

  getIsLoggedIn = () => {
    return this.localStorageService.getItem(UserConst.IS_LOGGED_IN);
  };

  setUserName = name => {
    this.localStorageService.setItem(UserConst.USER_NAME, name);
  };

  getUserName = () => {
    try {
      return this.user.member_name;
    } catch (e) {
      console.log(e);
    }
  };

  getEmail = () => {
    try {
      return this.user.email;
    } catch (e) {
      console.log('Email not found. ', e);
    }
  };

  getDomainName = () => {
    try {
      return this.user.domain;
    } catch (e) {
      console.log('Domain not found. ', e);
    }
  };

  getPermissions = () => {
    return this.user.permissions;
  };

  flushAll = () => {
    this.localStorageService.removeItem(UserConst.IS_LOGGED_IN);
    this.localStorageService.removeItem(UserConst.CURRENT_USER);
    this.localStorageService.removeItem(UserConst.TOKEN);
    this.localStorageService.clearAll();
  };

  setAll = user => {
    this.setIsLoggedIn(true);
    this.setToken(user.token);

    const userData = {
      default_project: user.default_project,
      default_project_type: user.default_project_type,
      domain: user.domain,
      email: user.email,
      image_url: user.image_url,
      member_name: user.member_name,
      token: user.token,
      is_loggedin: true,
      permissions: user.permissions
    };
    // this.setUser(this.EncrDecr.set(this.salt, userData));
    this.setUser(userData);
  };

  isLoggedIn() {
    if (
      this.localStorageService.hasKey(UserConst.IS_LOGGED_IN) &&
      this.localStorageService.getItem(UserConst.IS_LOGGED_IN) === 'true'
    ) {
      return true;
    }
    return false;
  }
}
