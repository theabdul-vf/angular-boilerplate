import {throwError as observableThrowError,  Observable, from } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import { User } from '../models/user';
import {appApiResources} from '../app.constants';

import { LocalStorageService } from '../services/local-storage.service';
import {UserService} from "./user.service";
import {DataTablesResponse} from "../models/datatable/datatables";

@Injectable()
export class AuthenticationService {
  private _url: string = appApiResources.login;
  private _url_registrer: string = appApiResources.register;
  private _url_token_validity = appApiResources.tokenvalidity;

  constructor(
    private http: HttpClient,
    private router: Router ,
    private localStorageService: LocalStorageService,
    private currentUser: UserService
  ) {  }

  login(subdomain, email_id, password): Observable<any> {
    return this.http.post<any>(this._url, { subdomain, email_id, password})
                    .pipe(map(user => {
                      if (user) {
                        this.currentUser.setAll(user);
                      }
                      return user;
                  }));
  }
  errorHandler(error: HttpErrorResponse){
    console.log(error);
    return observableThrowError(error.message || "Server Error");
  }

  logout() {
    this.currentUser.flushAll();
    this.router.navigateByUrl('/');
  }
  register(user) {
    return this.http.post<User[]>(this._url_registrer, user);
  }

  checkToken(): Observable<any> {
    const endPoint = `${this._url_token_validity}`;
    const param = {token: this.currentUser.getToken()}
    return this.http.post<DataTablesResponse>(endPoint, param)
      .pipe(tap(data => (JSON.stringify(data))) , catchError(this.errorHandler));
  }

}
