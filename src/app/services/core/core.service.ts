import { Injectable } from '@angular/core';
import { appRoutesURI } from '../../app.constants';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { CoreHelper } from '../../helpers/core.helper';
import { LabelService } from '../core/label.service';
import { ModuleDataService } from '../module/module.data.service';
import { UiBlockerService } from '../ui-blocker.service';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  public model: any = {};
  public filterModel: any = {};
  public tags = [];
  public appRoute;

  private isMenuBarShow = true;
  public menuClass = 'navi-icon open';
  public openSideBar = 'open-sidebar';
  public openMask = ''; //open-mask
  public openClass = '';
  private pageTitle;

  constructor(
    private currentUser: UserService,
    private router: Router,
    private coreHelper: CoreHelper,
    private labelService: LabelService,
    private moduleDataService: ModuleDataService,
    private uiBlockerService: UiBlockerService
  ) {
    this.appRoute = appRoutesURI;
    this.filterModel.status = null;
    this.filterModel.save_filters = null;
  }

  setIsMenuBarShow = isShow => {
    this.isMenuBarShow = isShow;
  };

  isShowSideMenu = () => {
    return this.isMenuBarShow;
  };
  onOpenMenu = () => {
    if (this.isShowSideMenu()) {
      this.menuClass = 'navi-icon open';
      this.openSideBar = 'open-sidebar';
      this.openMask = ''; //open-mask
      this.openClass = '';
    } else {
      this.menuClass = 'navi-icon';
      this.openSideBar = '';
      this.openMask = '';
      this.openClass = 'open';
    }
  };
  onMaskClick = () => {
    if (this.isShowSideMenu()) {
      this.menuClass = 'navi-icon open';
      this.openSideBar = 'open-sidebar';
      this.openMask = 'open-mask';
    } else {
      this.menuClass = 'navi-icon';
      this.openSideBar = '';
      this.openMask = '';
    }
  };
  isFilterSelected = () => {
    return this.tags.length > 0 ? true : false;
  };

  setPageTitle = title => {
    this.pageTitle = title;
  };
  getPageTitle = () => {
    return this.pageTitle;
  };
}
