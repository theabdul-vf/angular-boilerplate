
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class JsonService {

    constructor() {
    }

  doStringify = (data) => {
    return JSON.stringify(data);
  }

  doParse = (data) => {
    return JSON.parse(data);
  }
}
