
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LabelService {

  labels  = {
     project: {
        message: {
          'create': 'Project created successfully',
          'update': 'Project successfully updated',
          'remove': 'Project successfully deleted',
          'defaultProject': 'Default project updated successfully',
        },
        confirmation: {
          softDelete: 'Are you sure you want to delete this project?',
          restore: 'Are you sure you want to restore this project',
          hardDelete: 'Are you sure you want to delete this project permanently!'
        },
        button: {
          createLable: 'Save',
          updateLable: 'Update',
        },
        types:{
          web: 'web',
          rWeb: 'r-web',
          mobile: 'mobile',
          desktop: 'desktop',
       }
     },
    build: {
      message: {
        'create': 'Build created successfully',
        'update': 'Build successfully updated',
        'remove': 'Build successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this build?',
        restore: 'Are you sure you want to restore this build',
        hardDelete: 'Are you sure you want to delete this build permanently!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
      }
    },
    module: {
      message: {
        'create': 'Module created successfully',
        'update': 'Module successfully updated',
        'remove': 'Module successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this module?',
        restore: 'Are you sure you want to restore this module',
        hardDelete: 'Are you sure you want to delete this module permanently!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
        saveCreateLable: 'Save & Create New'
      }
    },
    testScenario: {
      message: {
        'create': 'Test Scenario created successfully',
        'update': 'Test Scenario successfully updated',
        'remove': 'Test Scenario successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this Test Scenario?',
        restore: 'Are you sure you want to restore this Test Scenario',
        hardDelete: 'Are you sure you want to delete this Test Scenario permanently!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
        saveCreateLable: 'Save & Create New'
      }
    },
    testCase: {
      message: {
        'create': 'Test Case created successfully',
        'update': 'Test Case successfully updated',
        'remove': 'Test Case successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this Test Case?',
        restore: 'Are you sure you want to restore this Test Case',
        hardDelete: 'Are you sure you want to delete this Test Case permanently!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
        saveCreateLable: 'Save & Create New'
      }
    },
    defect: {
      message: {
        'create': 'Defects created successfully',
        'update': 'Defects successfully updated',
        'remove': 'Defects successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this Defects?',
        restore: 'Are you sure you want to restore this Defects',
        hardDelete: 'Are you sure you want to delete this build Defects!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
        saveCreateLable: 'Save & Create New'
      }
    },
    cycle: {
      message: {
        'create': 'Cycle created successfully',
        'update': 'Cycle successfully updated',
        'remove': 'Cycle successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this Cycle?',
        restore: 'Are you sure you want to restore this Cycle',
        hardDelete: 'Are you sure you want to delete this Cycle permanently!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
      }
    },
    user: {
      message: {
        'create': 'User created successfully',
        'update': 'User successfully updated',
        'remove': 'User successfully deleted',
        'passwordUpdate': 'Your password update successfully.'
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this user?',
        restore: 'Are you sure you want to restore this user',
        hardDelete: 'Are you sure you want to delete this user permanently!'
      },
      button: {
        createLable: 'Create',
        updateLable: 'Update',
      }
    },
    customField: {
      message: {
        'create': 'Custom Field created successfully',
        'update': 'Custom Field successfully updated',
        'remove': 'Custom Field successfully deleted',
      },
      confirmation: {
        softDelete: 'Are you sure you want to delete this custom Field?',
        restore: 'Are you sure you want to restore this custom Field',
        hardDelete: 'Are you sure you want to delete this custom Field permanently!'
      },
      button: {
        createLable: 'Save',
        updateLable: 'Update',
      }
    },
     actionButtonLable: {
       create: 'Saving',
       update: 'Updating',
       cancel: 'Cancel'
     },
    dateTimeFormat: {
       dateFormat: 'YYYY-MM-DD'
    },
    tempModel: {
       name: 'tempModel'
    },
    export: {
      csv:'CSV',
      excel: 'Excel',
      word: 'Word',
      message: {
        exporting: 'Exporting inprogress...',
        exported: 'File is exported please click here to download.'
      }
    },
    modules: {
      testScenario: 'TestScenario',
      testCase: 'TestCase',
      defects: 'Defects',
      testCycle: 'TestCycle',
      testCycleExecution: 'TestCycleExecution',
    },
    auth: {
       tokeExpire: 'Token has been expired. Please sign in again.'
    }
   };
    constructor() {
    }
    getByItem = (key) => {
      return `this.labels.${key}`;
    }
}
