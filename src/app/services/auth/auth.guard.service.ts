import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../authentication.service';
import { first } from 'rxjs/operators';
import { ToastrNotificationService } from '../notification/toastr.notification.service';
import { LabelService } from '../core/label.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private router: Router,
    private currentUser: UserService,
    private authenticationService: AuthenticationService,
    private toastr: ToastrNotificationService,
    private labelService: LabelService
  ) {}

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.currentUser.isLoggedIn()) {
      let isLoggedIn = false;
      await this.authenticationService
        .checkToken()
        .pipe(first())
        .subscribe(
          data => {
            if (data.status) {
              return true;
            }
          },
          error => {
            this.toastr.showError(this.labelService.labels.auth.tokeExpire);
            this.router.navigate(['/logout']);
            return false;
          }
        );
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }
}
