import { Injectable } from '@angular/core';
import {UserService} from '../user.service';
import {Router} from "@angular/router";
import {CoreHelper} from "../../helpers/core.helper";
import {LabelService} from "../core/label.service";
import { UserDataService } from "./user.data.service";
import {first} from "rxjs/operators";
import {appRoutesURI} from "../../app.constants";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public title;
  public type;
  public submitted = false;
  public cancelButtonText;
  public buttonText;
  public model: any = {};
  public roles: any;
  public countries: any;
  public states: any;
  public appRoute = appRoutesURI.user;

  constructor(
    private currentUser: UserService,
    private router: Router,
    private coreHelper: CoreHelper,
    private labelService: LabelService,
    private userDataService: UserDataService
  ) {
    this.cancelButtonText = this.labelService.labels.actionButtonLable.cancel;
  }

  getType = () => {
    switch (this.type) {
      case 'inactive':
        return 1;
      case 'deleted':
        return 2;
      default:
        return 0;
    }
  }
  onFormRest = (f: any) => {
    this.coreHelper.resetForm(f);
  }

  onCancel = (f: any, url = null) => {
    this.onFormRest(f);
    this.router.navigate([url]);
  }


  getRoles = () => {
    this.userDataService.getRoles()
      .pipe(first())
      .subscribe(
        data => {
         this.roles = data.role_list;
        },
        errors => {

        });
  }

  getCountries = () => {
    this.userDataService.getCountries().subscribe(data => {
      this.countries = data;
    });
  }

  getState = (country) => {
    this.states = [];
      const filtered = this.countries.filter(function (item) {
        return item.code2 === country;
      });
      if(filtered && filtered[0]){
        this.states = filtered[0].states;
      }
  }

}
