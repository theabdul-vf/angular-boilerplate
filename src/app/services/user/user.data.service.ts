import { Injectable } from '@angular/core';
import {appApiResources} from '../../app.constants';
import {UserService} from '../user.service';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {HttpHelper} from "../../helpers/http.helper";
import {Observable, throwError as observableThrowError} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {JsonService} from "../core/json.service";
import {DataTablesResponse} from "../../models/datatable/datatables";
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})

export class UserDataService {

  private baseURL;
  private headers;

  constructor(
    private http: HttpClient,
    private currentUser: UserService,
    private httpHelper: HttpHelper,
    private jsonService: JsonService
  ) {
    this.baseURL = appApiResources.user;
    this.headers = this.httpHelper.getHttpHeaders();
  }

  getAll(param: any): Observable<any> {
    const endPoint = `${this.baseURL.list}`;
    return this.http.post<DataTablesResponse>(endPoint, param)
      .pipe(tap(data => (JSON.stringify(data))) , catchError(this.errorHandler));
  }

  getById(param: any): Observable<any> {
    const endPoint = `${this.baseURL.detail}?${param}`;
    return this.http.get<User[]>(endPoint, {headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandler));
  }

  onCreate(model: any): Observable<any> {
    const endPoint = `${this.baseURL.create}`;
    return this.http.post<User[]>(endPoint, model, {headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandlerMessage));
  }

  onUpdate(model: any): Observable<any> {
    const endPoint = `${this.baseURL.update}?token=${this.currentUser.getToken()}`;
    return this.http.post<User[]>(endPoint, model, {headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandlerMessage));
  }

  onDelete(param: any): Observable<any> {
    const endPoint = `${this.baseURL.trash}`;
    return this.http.post<User[]>(endPoint, param,{headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandlerMessage));
  }

  onActivation(user: any): Observable<any> {
    const endPoint = `${this.baseURL.activation}`;
    return this.http.post<User[]>(endPoint, user, {headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandler));
  }

  getRoles(): Observable<any> {
    const endPoint = `${this.baseURL.role}`;
    return this.http.post<any>(endPoint, {token: this.currentUser.getToken()}, {headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandler));
  }

  public getCountries(): Observable<any> {
    return this.http.get('./assets/data/countries.json')
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandler));
  }

  onPasswordUpdate(model: any): Observable<any> {
    const endPoint = `${this.baseURL.passwordUpdate}`;
    return this.http.post<any>(endPoint, model, {headers: this.headers})
      .pipe(tap(data => (this.jsonService.doStringify(data))) , catchError(this.errorHandlerMessage));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.error || 'Server Error');
  }

  errorHandlerMessage(error: HttpErrorResponse) {
    return observableThrowError(error.error || 'Server Error');
  }

}
