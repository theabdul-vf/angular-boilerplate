import { Injectable } from '@angular/core';
import { appApiResources, appRoutesURI } from '../../app.constants';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { CoreHelper } from '../../helpers/core.helper';
import * as moment from 'moment';
import { LabelService } from '../core/label.service';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {
  public isSaveButtonDisabled = false;
  public buttonText;
  public cancelButtonText;
  public saveCreateButtonText;
  public SAVE_TYPE = 'save';
  public SAVE_CREATE_TYPE = 'save_create';
  public submitted = false;
  public submittedSaveCreate = false;
  public model: any = {};
  public isError = false;
  public appRoute;
  public builds;

  constructor(
    private currentUser: UserService,
    private router: Router,
    private coreHelper: CoreHelper,
    private labelService: LabelService
  ) {
    this.appRoute = appRoutesURI;
  }

  onFormRest = (f: any) => {
    this.coreHelper.resetForm(f);
    this.model.build_id = 0;
  };

  onCancel = (f: any, url = null) => {
    this.onFormRest(f);
    this.router.navigate([url]);
  };
}
