import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { appApiResources } from '../../../app/app.constants';
import { UserService } from '../../../app/services/user.service';
import { HttpHelper } from '../../helpers/http.helper';
import { Module } from '../../models/module';
import { JsonService } from '../core/json.service';
import { DataTablesResponse } from '../../models/datatable/datatables';

@Injectable({
  providedIn: 'root'
})
export class ModuleDataService {
  private headers;
  private baseURL;

  constructor(
    private http: HttpClient,
    private currentUser: UserService,
    private httpHelper: HttpHelper,
    private jsonService: JsonService
  ) {
    // this.baseURL = appApiResources.module;
    this.headers = this.httpHelper.getHttpHeaders();
  }

  getAll(param: any): Observable<any> {
    const endPoint = `${this.baseURL.list}`;
    return this.http.post<DataTablesResponse>(endPoint, param).pipe(
      tap(data => JSON.stringify(data)),
      catchError(this.errorHandler)
    );
  }

  getById(param): Observable<any> {
    const endPoint = `${this.baseURL.detail}?${param}`;
    return this.http.get<Module[]>(endPoint, { headers: this.headers }).pipe(
      tap(data => this.jsonService.doStringify(data)),
      catchError(this.errorHandler)
    );
  }

  onCreate(model: any): Observable<any> {
    const endPoint = `${this.baseURL.create}`;
    return this.http
      .post<Module[]>(endPoint, model, { headers: this.headers })
      .pipe(
        tap(data => this.jsonService.doStringify(data)),
        catchError(this.errorHandlerMessage)
      );
  }

  onUpdate(model: any): Observable<any> {
    const endPoint = `${
      this.baseURL.update
    }?token=${this.currentUser.getToken()}`;
    return this.http
      .post<Module[]>(endPoint, model, { headers: this.headers })
      .pipe(
        tap(data => this.jsonService.doStringify(data)),
        catchError(this.errorHandlerMessage)
      );
  }

  onDelete(param: any): Observable<any> {
    const endPoint = `${this.baseURL.trash}`;
    return this.http
      .post<Module[]>(endPoint, param, { headers: this.headers })
      .pipe(
        tap(data => this.jsonService.doStringify(data)),
        catchError(this.errorHandlerMessage)
      );
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.error || 'Server Error');
  }

  errorHandlerMessage(error: HttpErrorResponse) {
    return observableThrowError(error.error || 'Server Error');
  }
}
