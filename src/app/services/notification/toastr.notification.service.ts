import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import {Router, NavigationStart, Route, ActivatedRoute} from '@angular/router';
import { Observable, Subject } from 'rxjs';
import {Toastr, ToastrManager} from 'ng6-toastr-notifications';

@Injectable({
  providedIn: 'root'
})
export class ToastrNotificationService {

    constructor(
      private router: Router,
      private _route: ActivatedRoute,
      public toastr: ToastrManager,
      private _location: Location
    ) {}

  toasterClickedHandler() {
    console.log('Toastr clicked');
    this.router.navigate([ '/projects/create' ], { queryParams: { order: 'popular' } });
  }

    showSuccess = (message) => {
      this.toastr.successToastr(message, 'Success!');
      this.toastr.onClickToast().subscribe( toast => {
        /*
        * Do some stuff here
        */
        console.log(toast.config);
        // window.location.href = 'projects/list/1340';

      });
    }

    showError = (message, callback = null) => {
      this.toastr.errorToastr(message, 'Oops!');
      this.toastr.onClickToast().subscribe(callback);
    }

    showWarning = (message) => {
      this.toastr.warningToastr(message, 'Alert!');
    }

    showInfo = (message, callback = null) => {
      this.toastr.infoToastr(message, 'Info', { enableHTML: true });
      this.toastr.onClickToast().subscribe(callback);
    }

    showCustom = () => {
      this.toastr.customToastr(
        `<span style='color: green; font-size: 16px; text-align: center;'>Custom Toast</span>`,
        null,
        { enableHTML: true }
      );
    }

    showToast(position: any = 'top-left') {
      this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
      });
    }
}
