var userInfo = JSON.parse(localStorage.getItem('currentUser'));
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class UserHelper {
    public userInfo;

    constructor() {
        this.userInfo = JSON.parse(localStorage.getItem('_currentUser'));
    }
    getUserToken() {
        return this.userInfo.token;

    }
    getDefaultProject() {
        return this.userInfo.default_project;

    }
    getMemberName() {
        return this.userInfo.member_name;

    }
    getUserEmail() {
        return this.userInfo.email;

    }
    getDomainName() {
        return this.userInfo.domain;
    }
    setMemberName(name:string) {
        this.userInfo.member_name = name;
    }
    setProjectID(projectId:number) {
        this.userInfo.default_project = projectId;
    }
    setUserToken(token:string) {
        this.userInfo.token = token;
    }
}
