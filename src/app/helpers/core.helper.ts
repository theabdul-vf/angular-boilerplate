import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class CoreHelper {
  getConfirmationBox(message: string = 'Are you want to delete!') {
    return confirm(message);
  }

  resetForm = (f) => {
    f.resetForm();
    f.reset();
  }

  modelFilter = (model) => {

  }

  openWindow = (url, target='_self') => {
    window.open(url, target);
  }

  openNewWindow = (url) => {
    this.openWindow(url,'_blank');
  }

  isEmptyObject = (obj) => {
    return Object.getOwnPropertyNames(obj).length === 0;
  }

}
