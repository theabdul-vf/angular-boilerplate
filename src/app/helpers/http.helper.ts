import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})


export class HttpHelper {
    private http;
    private headers;

    constructor(
      private httpClient: HttpClient,
      private router: Router,
      private route: ActivatedRoute
    ) {
      this.http = this.httpClient;
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*'
      });
    }

    getParam(param: any) {
      return this.route.snapshot.paramMap.get('id');
    }

    getHttp() {
      return this.http;
    }

    getHttpHeaders() {
      return this.headers;
    }

}
