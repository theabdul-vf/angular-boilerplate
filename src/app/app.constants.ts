import { environment } from '../environments/environment';

export let layoutView = {
  isSideBarShow: true,
  isHeaderShow: true,
  isFooterShow: true
};

function createUrl(actionName: string): string {
  return `${environment.API_BASE_URL}${actionName}`;
}

export function getBaseURL(domain: string): string {
  return `http://${domain}.${environment.BASE_URL}/`;
}

export const appApiResources = {
  baseUrl: environment.API_BASE_URL,
  tokenvalidity: createUrl('auth/tokenvalidity'),
  login: createUrl('auth/signin'),
  register: createUrl('auth/register'),
  user: {
    list: createUrl('users/list'),
    create: createUrl('users/create'),
    detail: createUrl('users/detail'),
    update: createUrl('users/update'),
    trash: createUrl('users/delete'),
    activation: createUrl('users/activation'),
    role: createUrl('roles/list'),
    passwordUpdate: createUrl('users/update_password')
  },
  dashboard: {}
};
export const appRoutesURI = {
  user: {
    list: 'users',
    deleted: 'users/deleted',
    inactive: 'users/inactive',
    create: 'users/create',
    edit: 'users/edit',
    activation: 'users/activation'
  }
};

export const appRoutes = {
  root: '',
  signup: 'signup',
  login: 'login',
  notFound: 'notfound',
  logout: 'logout',
  dashboard: 'dashboard',
  user: {
    userList: `${appRoutesURI.user.list}`,
    userDeletedList: `${appRoutesURI.user.deleted}`,
    userInactiveList: `${appRoutesURI.user.inactive}`,
    userCreate: `${appRoutesURI.user.create}`,
    userEdit: `${appRoutesURI.user.edit}/:id`,
    userActivation: `${appRoutesURI.user.activation}/:email/:code/:invite_page`
  }
};

export const appVariables = {
  userLocalStorage: 'currentUser',
  accessTokenServer: 'authorization',
  defaultContentTypeHeader: 'application/json',
  accessTokenLocalStorage: 'conduitAccessToken'
};

export const clientEnv = {
  public_token: '61d58922018e8fc57f94c56711d8f4'
};
