export interface DataTablesResponse {
  id?: number | null
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
  saveFilters: any;
}
