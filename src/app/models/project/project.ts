export interface Project {
  id?: number;
  project_id: number;
  project_name: string;
  project_type: string;
  project_status: string;
  created_on1: any;
  username: any;
  project_os: any;
  project_browser: any;
  project_devices: any;
  token: string;
  draw: number | null;
  recordsFiltered: number | null;
  recordsTotal: number | null;
  project_description: string;
  deleted_at: any
}
