export interface Module {
  id?: number;
  project_id: number;
  build_id: number;
  module_name: string;
  module_description: string;
  token: string;
  draw: number | null;
  recordsFiltered: number | null;
  recordsTotal: number | null;
  deleted_at: any;
  module_id: any;
  buildname: string;
  name: string;
  created_date: any;
  notes: any
}
