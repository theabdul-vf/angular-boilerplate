export interface User {
  first_name: string;
  last_name: string;
  email_id: string;
  email: string;
  username: string;
  profile_username: string;
  roles: string;
  street_1: string;
  street_2: string;
  city: string;
  state: string;
  zipcode: string;
  country: string;
  project_id: number;
  subdomain: string;
  password: string;
  default_project: string;
  token: string;
  permissions: any;
}
