export interface DefectBySeverityModel {
  country: string;
  medals: number;
}
