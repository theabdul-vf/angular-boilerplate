export interface Population {
  state: string;
  failed: number;
  notRun: number;
  paused: number;
}
