export interface CycleModel {
  defects: string;
  passed: number;
  failed: number;
}
