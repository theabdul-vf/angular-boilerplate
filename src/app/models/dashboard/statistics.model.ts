export interface Statistics {
  month: string;
  archived: number;
  completed?: number;
  ongoing?: number;
}
