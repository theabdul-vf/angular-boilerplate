export interface Counter {
  token: string;
  project_id: number;
  module_id: number;
  build_id: number;

  ts_module_id: number;
  ts_build_id: number;
  ts_requirement_id: number;

  tc_module_id: number;
  tc_build_id: number;
  tc_requirement_id: number;
  tc_status : string;
  tc_type: string;
  tc_scenario_id: number;

  defect_module_id: number;
  defect_build_id: number;
  defect_browser: string;
  defect_os : string;
  defect_severity: string;
  defect_status: string;
  defect_assignto: number;
  defect_type: string;
 }
