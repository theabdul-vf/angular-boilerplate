import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';
import { AlertService } from '../../services/alert.service';
import { UiBlockerService } from '../../services/ui-blocker.service';
import { layoutView, appRoutes, getBaseURL } from '../../app.constants';
import { ToastrNotificationService } from '../../services/notification/toastr.notification.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loading = false;
  returnUrl: string;
  message = false;
  isHeaderShow = (layoutView.isHeaderShow = true);
  isSideBarShow = (layoutView.isSideBarShow = false);
  isFooterShow = (layoutView.isFooterShow = false);

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private uiBlockerService: UiBlockerService,
    private toastr: ToastrNotificationService,
    private currentUser: UserService
  ) {}

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  ngOnInit() {
    console.log('hello login here');
    if (this.currentUser.isLoggedIn()) {
      // this.router.navigate([appRoutes.project.projects]);
    }
    // reset login status
    // this.authenticationService.logout();
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.uiBlockerService.start();
    this.loading = true;
    const subDomain = this.f.subdomain.value;
    this.authenticationService
      .login(subDomain, this.f.email_id.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          const userData = JSON.stringify(data);
          const userDataInfo = JSON.parse(userData);
          this.loading = false;
          this.router.navigate([appRoutes.dashboard]);
          this.uiBlockerService.stop();
        },
        errors => {
          // this.alertService.error(errors.error.errors);
          this.toastr.showError('Username or Password invaid.');
          this.loading = false;
          this.message = true;
          this.uiBlockerService.stop();
        }
      );
  }
  onLogout() {
    this.authenticationService.logout();
  }
}
